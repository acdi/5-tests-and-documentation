# -*- coding: utf-8 -*-
"""Tests for input.py submodule

Author
------
Jenni Rinker
rink@dtu.dk
"""

import pytest

from mytestdocs.input import get_int


def test_get_int_value(monkeypatch):
    """Check that the correct value is returned
    """
    # given
    x_theo = 4
    # monkeypatch the "input" function, so that it returns x_theo.
    # (This simulates the user entering the value in the terminal.)
    monkeypatch.setattr('builtins.input', lambda x: x_theo)
    # when
    x = get_int()  # call function that relies on monkeypatched input()
    # then
    assert x == x_theo


def test_get_int_inputerror(monkeypatch):
    """Check that an error is raised when bad input is given
    """
    # given
    x_theo = 'cat'
    # monkeypatch the "input" function, so that it returns x_theo.
    # (This simulates the user entering the value in the terminal.)
    monkeypatch.setattr('builtins.input', lambda x: x_theo)
    # when and then
    with pytest.raises(ValueError):
        get_int()  # error should be raised
